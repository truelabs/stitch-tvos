(function(root) {
"use strict";

var Templates = {
    Loading: Loading,
    UnsubscribeAlert: UnsubscribeAlert,
    EventList: EventList,
    Join: Join,
    Event: Event,

    // internal functions that need to be access from Template functions that do
    // not run in this context.
    _: {
        eventListOnLoad: eventListOnLoad,
        joinOnLoad: joinOnLoad,
        eventOnLoad: eventOnLoad,
        eventItemOnClick: eventItemOnClick,
        eventSlideshowOnClick: eventSlideshowOnClick,
        unsubscribeConfirmOnClick: unsubscribeConfirmOnClick,
    }
};

root.Templates = Templates;


function UnsubscribeAlert(eventID) {
    return `<?xml version="1.0" encoding="UTF-8" ?>
<document>
  <head>
    <style>
      .lightBackgroundColor {
        background-color: #4DA1FF;
      }
      .destructive {
        background-color: #FF6666;
        tv-highlight-color: red;
      }
    </style>
  </head>
  <alertTemplate class="lightBackgroundColor" >
    <title>Confirmation</title>
    <description>Are you sure you want to unfollow this event?</description>
    <button onclick="Templates._.unsubscribeConfirmOnClick('${eventID}');" class="destructive">
      <text>Yes</text>
    </button>
    <button onclick="navigationDocument.popDocument();">
      <text>Cancel</text>
    </button>
  </alertTemplate>
</document>`
}

function unsubscribeConfirmOnClick(eventId) {
    removeFromRecentEvents(eventId);
    navigationDocument.popToRootDocument(); 
    GoogleAnalytics.event("ui_action", "unsubscribe");
}

function eventListChangedHandler(event) {
    console.debug("eventListChangedHandler", event);
    var ownerDocument = event.target.ownerDocument;
    var doc = Presenter.parseAndSetDefaultEventHandlers(EventList());
    if (navigationDocument.documents.indexOf(ownerDocument) !== -1) {
        console.debug("replacing event list document");

        // install the change handler now, because onload won't run until the page is displayed
        // but if the user changes the event list between now and then, we still need to respond
        // to those changes.
        installEventListChangedHandler(doc);

        navigationDocument.replaceDocument(doc, ownerDocument);
    } else {
        console.warn("not replacing document, owner no longer in stack.");
    }
}

function installEventListChangedHandler(doc) {
    console.log("installEventListChangeHandler");
    doc.addEventListener("eventListChanged", eventListChangedHandler);
}

function eventListOnLoad(event) {
    installEventListChangedHandler(event.target.ownerDocument);
    GoogleAnalytics.screenview("event_list");
}

function EventList() {
    var events = JSON.parse(localStorage.getItem('eventList'));
    var joinListItem = `
<listItemLockup template="Templates.Join">
    <title class="subscribe"><badge src="resource://button-add" />  Follow Event</title>
    <relatedContent>
    <lockup>
        <img src="${R('images/parade-icon.png')}" width="1024" height="1024" />
        <description class="descriptionLayout">Follow a shared event created with the Stitch app and view photos and videos on your Apple TV.</description>
    </lockup>
    </relatedContent>
</listItemLockup>`;

    var eventListItems = "";

    for(var i = 0, len = events.length; i < len; i++) {
        var imageElements = "";
        var event = events[i];
        var images = event.images;
        if (images) {
          for(var j = 0; j < 4; j++) {
              var img = images[j];
              if (img) { 
                  imageElements += `<img src="${img}" />`;
              }
          }
        }

        eventListItems += `
<listItemLockup template="Templates.Event.bind(this, '${event.eventID}')">
    <title>${event.eventName}</title>
    <relatedContent><imgDeck>
${imageElements}
    </imgDeck></relatedContent>
</listItemLockup>`;
    }

    return `<?xml version="1.0" encoding="UTF-8" ?>
<document onload="Templates._.eventListOnLoad(event)">
  <head>
    <style>
    .templateBackground {
      background-color: #4DA1FF;
    }
    .subscribe {
      color: #0373F0;
    }
    </style>
  </head>
  <paradeTemplate class="templateBackground">
    <list>
      <header>
        <title>Events</title>
      </header>
      <section>` +
          joinListItem + eventListItems +
`     </section>
    </list>
  </paradeTemplate>
</document>`
}

function tryToPair(doc, code) {

    if (doc.stopTryingToPair) {
        console.debug("stop trying to pair flag set.");
        return;
    }

    var pairingRetryDelayInMS = 2 * 1000;

    StitchAPI.pairingCodeValue(code).then(function(result) {
        if (result.status >= 200 && result.status < 300) {
            var eventId = result.value;
            if (eventId && eventId.length > 0) {
                // got a value, use it
                console.log("Paired with eventId", eventId);
                GoogleAnalytics.event("ui_action", "subscribed");
                Presenter.processTemplate(Templates.Event.bind(this, eventId)).then(function(eventDoc) {
                    navigationDocument.replaceDocument(eventDoc, doc);
                });
            } else {
                // keep trying
                console.log("Successful result, but no code paired. Trying again.");
                setTimeout(function() { tryToPair(doc, code); }, pairingRetryDelayInMS);
            }
        } else {
            console.error("Server returned value. Giving up.", result.status);
            // TODO: alert user
        }
    }).catch(function(reason) {
        console.error("Failure while trying to pair. Giving up.", reason);
        // TODO: alert user
    });
}

function joinOnLoad(event, code) {
    tryToPair(event.target.ownerDocument, code);
    GoogleAnalytics.screenview("join");
}

function Join() {
    return StitchAPI.newPairingCode().then(function(code) {
        console.log("Got pairing code", code)
    return `<?xml version="1.0" encoding="UTF-8" ?>
<document onload="Templates._.joinOnLoad(event, '${code}')" onunload="event.target.ownerDocument.stopTryingToPair = true;">
  <head>
    <style>
      .lightBackgroundColor {
        background-color: #4DA1FF;
      }
      .titleOne {
        tv-text-style: title1;
      }
      .subtitleOne {
        tv-text-style: subtitle1;
      }
    </style>
  </head>
<descriptiveAlertTemplate class="lightBackgroundColor">
   <title class="titleOne">Follow Event</title>
   <description>To follow an event, you need the Stitch app for iOS or web.
Don&#39;t have Stitch for iOS? Go to http://get.stitchpic.us/

1. Go to a shared event in the Stitch app for iOS or web
2. Select Menu or  &#9776;  icon in top-right corner
3. Select "Display on Apple TV"
4. Enter subscription code below in the Stitch app for iOS or web
    </description>
   <text class="titleOne">${code}</text>
   <text class="subtitleOne">Subscription Code</text>
</descriptiveAlertTemplate>

</document>`
    }).catch(function() {
        console.error("Error getting pairing code.")
        var alert = createAlert("Error loading", "Failed to get pairing code. Please try again later.")
        navigationDocument.presentModal(alert)
    });
}

function eventItemOnClick(e, indexInEvent) {
    GoogleAnalytics.event("ui_action", "select_photo");

    var eventJSON = e.target.ownerDocument.eventJSON;
    loadOneup(eventJSON, indexInEvent, false, 4);
}

function eventGrid(event) {
    var out = "<grid>\n";
    out += "<section>\n";

    event.captures.forEach(function(cap, i) {
        var lockup = "";
        var bestImage = Variant.bestThumbnailForWidthAndHeight(cap, 390, 390);
        if (cap.transcodes){
            lockup = `
<lockup onclick="Templates._.eventItemOnClick(event, ${i});" accessibilityText="Video">
    <img src="${bestImage.url}" width="390" height="390" />
    <overlay><badge class="play" src="resource://button-play" /></overlay>
    <title>Shared by ${cap.owner_name}</title>
</lockup>`;
        } else {
            lockup = `
<lockup onclick="Templates._.eventItemOnClick(event, ${i});" accessibilityText="Photo">
    <img src="${bestImage.url}" width="390" height="390" />
    <overlay></overlay>
    <title>Shared by ${cap.owner_name}</title>
</lockup>`;
        }
        out += lockup;
    });

    out += "</section>\n</grid>";
    return out;
}

// used to transfer event from Event template function to eventOnLoad
var hackEventJSON;

function eventOnLoad(e) {
    e.target.ownerDocument.eventJSON = hackEventJSON;
    GoogleAnalytics.screenview("Event");
}

function eventSlideshowOnClick(e) {
    GoogleAnalytics.event("ui_action", "select_slideshow");
    var eventJSON = e.target.ownerDocument.eventJSON;
    loadOneup(eventJSON, 0, true, 4);
}

function Event(eventID) {
    return StitchAPI.getEvent(eventID).then(function(eventJSON) {
        hackEventJSON = eventJSON;

        //Update URLs for parade on home screen
        updateRecentEvents(eventJSON);

        var slideshowButton = `
<buttonLockup onclick="Templates._.eventSlideshowOnClick(event)" accessibilityText="Start Slideshow">
    <badge src="resource://button-play" />
    <title>Slideshow</title>
</buttonLockup>`;

        if (eventJSON.captures.length === 0 || Device.appVersion < 3) {
            // slideshow not great until build 3, so don't show it.
            slideshowButton = "";
        }

        return `<?xml version="1.0" encoding="UTF-8" ?>
<document onload="Templates._.eventOnLoad(event)">
  <head>
    <style>
      .lightBackgroundColor {
        background-color: #4DA1FF;
      }
      .play {
        margin: 195;
        tv-tint-color: rgb(255, 255, 255);
        width: 50;
        height: 50;
      }
      .eventTitle {
        text-align: center;
      }
    </style>
  </head>
  <stackTemplate theme="light" class="lightBackgroundColor" >
    <identityBanner>
      <title class="eventTitle">${eventJSON.event.name}</title>
      <row>
        ${slideshowButton}
        <buttonLockup template="Templates.UnsubscribeAlert.bind(this, '${eventID}')" accessibilityText="Unfollow Event">
            <badge src="resource://button-remove" />
            <title>Unfollow</title>
        </buttonLockup>
      </row>
    </identityBanner>
    <collectionList>
    ${eventGrid(eventJSON)}
    </collectionList>
  </stackTemplate>
</document>`
    });
}

function Loading() {
   return `<?xml version="1.0" encoding="UTF-8" ?>
<document>
<loadingTemplate>
<activityIndicator>
<text>Loading...</text>
</activityIndicator>
</loadingTemplate>
</document>`
}

// json is the event JSON
// index is where the slide show should start
// interval is time between slides
function loadOneup(eventJSON, index, autoadvance, interval) {
    var json = eventJSON;
    var itemsArray = [];
    for(var i = 0; i < json.captures.length; i++) {
        if (json.captures[i].transcodes){
            var itemURL = Variant.bestVideoForWidthAndHeight(json.captures[i], 1920, 1080).url;
            itemsArray.push({ URL: itemURL, isVideo: true })
        } else {
            var itemURL = Variant.bestThumbnailForWidthAndHeight(json.captures[i], 1920, 1080).url;
            itemsArray.push({ URL: itemURL, isVideo: false })
        }
    }

    GoogleAnalytics.screenview("oneup");
    if (Device.appVersion < 4){
      pushMyView(itemsArray, autoadvance, index);
    } else {
      pushMyView(itemsArray, autoadvance, index, interval);
    }
} 

})(this);

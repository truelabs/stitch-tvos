#!/bin/bash

DIR=debug

build() {
    ./build.sh "$DIR"
}

echo "Initial build"
build

pushd "$DIR" > /dev/null
python -m SimpleHTTPServer 9001 &
SERVER_PID=$!
popd > /dev/null

trap "kill $SERVER_PID" SIGINT SIGTERM EXIT

fswatch --one-per-batch js images | while read -r file; do
    echo "Files change. Rebuilding."
    build
done


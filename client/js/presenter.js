(function(root) {
    "use strict";

    var parser = new DOMParser();
    function parseXML(xml) {
        return parser.parseFromString(xml, "application/xml");
    }

    function parseAndSetDefaultEventHandlers(xml) {
        var doc = parseXML(xml);
        doc.addEventListener("select", load);
        return doc;
    }

    // If o is a Promise, return it. Otherwise, wrap it in a promise that resolve to o.
    function promise(o) {
        if (typeof o.then !== "function") {
            return new Promise(function(resolve, reject) {
                resolve(o);
            });
        }
        return o;
    }

    // Return a promise that resolve to a doc. template is a function that returns
    // either XML or a Promise that resolves to XML.
    function processTemplate(template) {
        return promise(template()).then(function(xml) {
            return parseAndSetDefaultEventHandlers(xml);
        });
    }

    // loadTemplate takes a template function  just like processTemplate, but also handles
    // showing a loading indicator if necessary and pushing the document on the stack.
    function loadTemplate(template) {
        var showLoadingIndicator = true;
        var templatePromise = processTemplate(template);
        var loadingIndicator;

        setTimeout(function() {
            console.debug("loadTemplate loading indicator timeout");
            if (!showLoadingIndicator) {
                console.debug("not showing loading indicator");
                return;
            }
            console.debug("showing loading indicator");
            loadingIndicator = parseXML(Templates.Loading());
            navigationDocument.pushDocument(loadingIndicator);
        }, 500);

        processTemplate(template).then(function(doc) {
            showLoadingIndicator = false;
            if(loadingIndicator) {
                navigationDocument.replaceDocument(doc, loadingIndicator);
            } else {
                navigationDocument.pushDocument(doc);
            }
        }).catch(function(reason) {
            showLoadingIndicator = false;
            console.error("Failed to load page.", reason);
            var title = "Error Loading Page",
                description = `Please try again later.`,
                alert = createAlert(title, description);
            if(loadingIndicator) {
                navigationDocument.removeDocument(loadingIndicator);
            }
            navigationDocument.presentModal(alert);
        });
    }

    function handleOnClickEvent(script, event) {
        eval(script);
    }

    // Select event handler
    function load(event) {
        var self = this,
            ele = event.target,
            template = ele.getAttribute("template"),
            onclick = ele.getAttribute("onclick");

        if (onclick) {
            handleOnClickEvent(onclick, event);
        } else if (template) {
            var templateFunction = eval(template);
            loadTemplate(templateFunction);
        }
    }

    var Presenter = {
        processTemplate: processTemplate,
        loadTemplate: loadTemplate,
        load: load,
        parseAndSetDefaultEventHandlers: parseAndSetDefaultEventHandlers,
    }

    root.Presenter = Presenter;
})(this);

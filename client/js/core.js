(function(root) {
    "use strict";

    var core = {};

    function runScripts(scripts) {
        return new Promise(function(resolve, reject) {
            evaluateScripts(scripts, function(success) {
                if (success) {
                    resolve();
                } else {
                    reject();
                }
            });
        });
    }

    root.core = {
        runScripts: runScripts,
    };
})(this);

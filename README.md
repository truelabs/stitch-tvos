# stitch-tvos
Stitch on Apple TV.

To run a test server you must:

    cd client
    ./debug.sh

Update AppDelegate.swift to point at your local web server.

To deploy server you must:

    cd client
    ./build.sh site
    ./deploy.sh site gs://tv.funsocialapps.com
(function(root) {
    "use strict";

    App.onLaunch = function(options) {
        console.debug("onLaunch", options);
        root.BASEURL = options.BASEURL;
        root.R = function(relativePath) {
            // remove leading forward slashes
            var path = relativePath.replace(/^\/+/, "");
            return `${options.BASEURL}${path}`;
        };
        main();
    };

    App.onExit = function(options) {
        console.debug("onExit", options);
    };

    App.onError = function(options) {
        console.debug("onError", options);
    }

    var maxDelayMilliseconds = 30 * 1000;
    var delayBase = 200;
    function registerDeviceWithRetry(retryCount) {
        retryCount = retryCount || 0;
        var identity = deviceIdentity();
        StitchAPI.registerDevice(identity.id, identity.password, "Apple TV").then(function() {
            console.log("Device registered.");
        }).catch(function() {
            var delay = Math.min(maxDelayMilliseconds, delayBase*Math.pow(2, retryCount));
            console.error("Failed to register device. Will try again in", delay, "ms.");
            setTimeout(function() { registerDeviceWithRetry(retryCount+1); }, delay);
        });
    }

    function updateRecentEvents(event) {
        var img = [];
        var count = 4;
        if (event.captures.length < 4) {
            count = event.captures.length;
        }
        for (var i = 0; i < count; i++) {
            var bestImageURL = Variant.bestThumbnailForWidthAndHeight(event.captures[i], 1920, 1080).url;
            img.push(bestImageURL);
        }

        var newEntry = {
            eventID: event.event.id,
            eventName: event.event.name,
            images: img
        };

        var list = JSON.parse(localStorage.getItem('eventList')); 
        var found = false;
        var changed = false;
        for (var i = 0; i < list.length; i++ ) {
            if (list[i].eventID === newEntry.eventID) {
                if (list[i].eventName != event.event.name || list[i].images[0] != img[0]) {
                    changed = true;
                }
                list[i] = newEntry;
                found = true;
                break;
            }
        }

        if (!found) {
            list.unshift(newEntry);
            changed = true;
        }

        localStorage.setItem('eventList', JSON.stringify(list));
        if (changed) {
            eventListChanged();
        }
    }
    root.updateRecentEvents = updateRecentEvents;

    function removeFromRecentEvents(eventID) {
        var list = JSON.parse(localStorage.getItem('eventList')); 
        var newList = list.filter(function(entry) {
            return entry.eventID !== eventID;
        });
        localStorage.setItem('eventList', JSON.stringify(newList));
        if (list.length !== newList.length) {
            eventListChanged();
        }
    }
    root.removeFromRecentEvents = removeFromRecentEvents;

    function dispatchEventToAllDocs(eventName) {
        console.log("dispatching event to all docs", event);
        // shallow copy docs so we don't have to worry about navigation stack mutations.
        var docs = navigationDocument.documents.slice();
        for(var doc of docs) {
            // Make a new Event for each document (i.e. don't share it between dispatchEvent calls),
            // otherwise you'll get weird behavior, like the first document's handler called for
            // each subsequent dispatch event.
            var event = new Event(eventName);
            doc.dispatchEvent(event);
        }
    }

    function eventListChanged() {
        console.log("got changed");
        dispatchEventToAllDocs("eventListChanged");
    }

    function main() {
        GoogleAnalytics.init("UA-60108717-8", "StitchTV");

        StitchAPI.onNeedsCredentials = onNeedsCredentials;
        registerDeviceWithRetry();

        //New user install
        var setupObject = localStorage.getItem('setupComplete');
        if (!setupObject) {
            console.log("New install... entering setup");
            var sampleObject = localStorage.getItem('eventList');
            if (!sampleObject) {
                console.log("No event list, inserting sample data.");
                //Sample data
                sampleObject = [
                    {'eventID': 'eb42a085-6aa8-4d92-8426-342dd0066436', 'eventName': 'Italy Trip', 'images': {
                     '0': 'https://storage.googleapis.com/stitch/204f10e3-b6a2-4ee9-9fd9-d4c7c1ccb7ed%2Feb42a085-6aa8-4d92-8426-342dd0066436%2Fd095adec210e8049276593fb662570bb.lte_4194304_pixels',
                     '1': 'https://storage.googleapis.com/stitch/204f10e3-b6a2-4ee9-9fd9-d4c7c1ccb7ed%2Feb42a085-6aa8-4d92-8426-342dd0066436%2Fc4d847b76b84e89a7f61894fb06e533e.lte_4194304_pixels',
                     '2': 'https://storage.googleapis.com/stitch/204f10e3-b6a2-4ee9-9fd9-d4c7c1ccb7ed%2Feb42a085-6aa8-4d92-8426-342dd0066436%2F466a206deb7973c040097ec887f8e8c2.lte_1048576_pixels',
                     '3': 'https://storage.googleapis.com/stitch/204f10e3-b6a2-4ee9-9fd9-d4c7c1ccb7ed%2Feb42a085-6aa8-4d92-8426-342dd0066436%2F758208a15969dd896c4004012138555f.lte_4194304_pixels'}
                    }
                ];
            localStorage.setItem('eventList', JSON.stringify(sampleObject));
            localStorage.setItem('setupComplete', 1);
            }
        }

        Presenter.loadTemplate(Templates.EventList);
    }

    /**
     * This convenience funnction returns an alert template, which can be used to present errors to the user.
     */
    var createAlert = function(title, description) {

        var alertString = `<?xml version="1.0" encoding="UTF-8" ?>
        <document>
        <alertTemplate>
        <title>${title}</title>
        <description>${description}</description>
        </alertTemplate>
        </document>`

        var parser = new DOMParser();

        var alertDoc = parser.parseFromString(alertString, "application/xml");

        return alertDoc
    }

    root.createAlert = createAlert;

    function deviceIdentity() {
        var id = localStorage.getItem("device-id")
        var password = localStorage.getItem("device-password")

        if (!id || !password) {
            console.debug("Generating new device ID and password.")
            id = uuid.v4()
            password = uuid.v4()
            localStorage.setItem("device-id", id)
            localStorage.setItem("device-password", password)
        }

        return { id: id, password: password }
    }

    function onNeedsCredentials() {
        return new Promise(function(resolve) {
            resolve(deviceIdentity());
        });
    }
})(this);


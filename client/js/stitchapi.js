/*
Copyright (C) 2015 True Labs, Inc. All Rights Reserved.

Abstract:
API to talk to Stitch Backend

*/

// StitchAPI
(function (root) {
    "use strict";

    var logXHRs = true;

    function xhrLogRequest(method, url, headers, data, xhr) {
        logXHRs && console.debug("XHR REQ: " + method + " " + url, "headers:", headers, "data:", data, xhr);
    }

    function xhrLogResponse(method, url, status, xhr) {
        logXHRs && console.debug("XHR RES: " + method + " " + url + " " + status, xhr);
    }

    function ajax(settings) {
        var method = settings.method || "GET";
        var url = settings.url;
        var headers = settings.headers || {};
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4) {
                    xhrLogResponse(method, url, xhr.status, xhr);
                    if (xhr.status >= 200 && xhr.status < 300) {
                        resolve(xhr);
                    } else {
                        reject(xhr);
                    }
                }
            };
            xhr.open(method, url);
            if (settings.contentType) {
                xhr.setRequestHeader("Content-Type", settings.contentType);
            }
            for(var k in headers) {
                xhr.setRequestHeader(k, headers[k]);
            }
            xhrLogRequest(method, url, headers, settings.data, xhr);
            xhr.send(settings.data);
        });
    }

    function authRequestHeaders() {
        return api.onNeedsCredentials().then(function(identity) {
            return {"Device-Id" : identity.id, "Device-Password" : identity.password}
        });
    }

    function authenticatedAjax(settings) {
        return authRequestHeaders().then(function(authHeaders) {
            console.log("auth headers", authHeaders);
            var headers = settings.headers || {};
            for(var k in authHeaders) {
                headers[k] = authHeaders[k];
            }
            settings.headers = headers;
            console.log("settings",settings);
            return ajax(settings);
        });
    }

    function registerDevice(id, password, username) {
        var data = { username: username, password: password };
        return ajax({
            url: baseURL + "/devices/" + id,
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
        });
    }

    function getJSON(url) {
        return ajax({"url": url}).then(function(xhr) {
            // JSON.parse throws on failure, which causes the promise to be rejected.
            return JSON.parse(xhr.responseText);
        });
    }

    var baseURL = "https://stitch-1026.appspot.com";

    function getEvent(eventID) {
        var url = baseURL + "/events/" + eventID;
        return getJSON(url).then(function(event) {
            event.captures.sort(function(a,b) {
                if (a.time > b.time) {
                    return -1
                } else if (a.time < b.time) {
                    return 1
                } else {
                    return 0
                }
            });
            return event;
        });
    }

    var pairCodesURL = baseURL + "/pair/codes";

    function newPairingCode() {
        return authenticatedAjax({method: "POST", url: pairCodesURL}).then(function(xhr) {
            return xhr.responseText;
        });
    }

    function pairingCodeValue(code) {
        var url = pairCodesURL + "/" + code + "/value";
        return authenticatedAjax({url: url}).then(function(xhr) {
            return { value: xhr.responseText, status: xhr.status };
        });
    }

    var api = {
        // implemented by the application. Application should return a promise that
        // resolves to a dictionary with an id and password properties.
        onNeedsCredentials: function() {
            console.error("implement me");
            return new Promise(function(res,reject) { reject(); });
        },

        registerDevice: registerDevice,
        getEvent: getEvent,
        newPairingCode: newPairingCode,
        pairingCodeValue: pairingCodeValue,
    };

    root.StitchAPI = api;
    
})(this); // StitchAPI



// Variant
(function(root) {
    "use strict";

    function bestVariantForTargetSize(variants, targetPixels) {
        var best = null;
        var bestPixels = 0;
        var variantLength = variants.length;
        for (var i = 0; i < variantLength; ++i) {
            var variant = variants[i];
            var variantPixels = variant.width * variant.height;

            if (best === null) {
                best = variant;
                bestPixels = variantPixels;
            } else {
                if (Math.abs(variantPixels - targetPixels) < Math.abs(bestPixels - targetPixels)) {
                    best = variant;
                    bestPixels = variantPixels;
                }
            }
        }

        return best;
    }

    function bestThumbnailForWidthAndHeight(capture, width, height) {
        return this.bestVariantForTargetSize(capture.thumbnails, width*height);
    }

    function bestVideoForWidthAndHeight(capture, width, height) {
        return this.bestVariantForTargetSize(capture.transcodes, width*height);
    }

    root.Variant = {
        bestVariantForTargetSize: bestVariantForTargetSize,
        bestThumbnailForWidthAndHeight: bestThumbnailForWidthAndHeight,
        bestVideoForWidthAndHeight: bestVideoForWidthAndHeight
    };
})(this); // Variant

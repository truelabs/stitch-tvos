//
//  OneUpController.swift
//  StitchTV
//
//  Created by Brian Ganninger on 11/2/15.
//  Copyright 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import AVFoundation

// Responsible for managing the data presented in a collection view as 1-Up + Slideshow (auto-advance + cycling)
class OneUpController : UIViewController {
    private let imageCellReuseId = "ImageCell"
    private let videoCellReuseId = "VideoCell"
    private let collectionView : UICollectionView
    
    struct ContentItem {
        let url: String
        let isVideo: Bool
        
        init(url: String, isVideo: Bool) {
            self.isVideo = isVideo
            self.url = url
        }
    }

	var photoDisplayDuration: NSTimeInterval = 3
	var currentContentItemIndex = 0
	var contentArray : [ContentItem] = []
	var currentSlideshowTimer: NSTimer? = nil
	var isRunningSlideshow = false
	
	init() {
		let layout = UICollectionViewFlowLayout()
		layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
		layout.minimumInteritemSpacing = 0
		layout.minimumLineSpacing = 0
		layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
		
        collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: layout)
        collectionView.userInteractionEnabled = false
        
        super.init(nibName: nil, bundle: nil)
        
        collectionView.delegate = self
        collectionView.dataSource = self
		
		layout.itemSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)
	}
    
    required init?(coder aDecoder: NSCoder) {
        assert(false)
        collectionView = UICollectionView()
        super.init(coder: aDecoder)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.registerClass(ImageCell.self, forCellWithReuseIdentifier: imageCellReuseId)
        collectionView.registerClass(VideoCell.self, forCellWithReuseIdentifier: videoCellReuseId)
		collectionView.backgroundColor = UIColor.blackColor()
        
        collectionView.frame = view.bounds
        collectionView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        collectionView.dataSource = self
        collectionView.delegate = self
        view.addSubview(collectionView)

        let swipeRecognizerRight = UISwipeGestureRecognizer(target: self, action: "swipeRight:")
        swipeRecognizerRight.direction = .Right
        view.addGestureRecognizer(swipeRecognizerRight)
        
        let swipeRecognizerLeft = UISwipeGestureRecognizer(target: self, action: "swipeLeft:")
        swipeRecognizerLeft.direction = .Left
        view.addGestureRecognizer(swipeRecognizerLeft)
	}
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "videoFinishedPlaying:", name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeSlideshowTimer()
        NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
    }
}

// Custom event handling
extension OneUpController {
    override func pressesEnded(presses: Set<UIPress>, withEvent event: UIPressesEvent?) {
        for item in presses {
            switch item.type {
            case .UpArrow:
                break
            case .DownArrow:
                break
            case .LeftArrow:
                previousContentItem()
            case .RightArrow:
                nextContentItem()
            case .PlayPause:
                if isRunningSlideshow {
                    stopSlideshow()
                } else {
                    startSlideshow()
                }
            case .Select:
                break
            case .Menu:
                break
            }
        }
    }
    
    func swipeLeft(sender : AnyObject) {
        nextContentItem()
    }
    
    func swipeRight(sender : AnyObject) {
        previousContentItem()
    }
}

extension OneUpController : UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contentArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cellContent = contentArray[indexPath.row]
        
        if cellContent.isVideo {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(videoCellReuseId, forIndexPath: indexPath) as! VideoCell
            cell.setVideoURL(cellContent.url)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(imageCellReuseId, forIndexPath: indexPath) as! ImageCell
            cell.tag = indexPath.row
            cell.delegate = self
            cell.setImageURL(cellContent.url)
            return cell
        }
    }
}

extension OneUpController : UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if let videoCell = cell as? VideoCell {
            videoCell.player.pause()
        }
        
        if let imageCell = cell as? ImageCell {
            // clear the delegate
            imageCell.delegate = nil
            imageCell.cancelImageDownload()
        }
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        if let videoCell = cell as? VideoCell {
            videoCell.player.play()
        } else if let imageCell = cell as? ImageCell {
            if imageCell.imageView.image != nil {
                if isRunningSlideshow {
                    addPhotoAdvanceTimer()
                }
                imageCell.delegate = nil
            } else {
                // wait for image loading to complete
                imageCell.delegate = self
            }
        }
        
        let next=indexPath.row + 1
        if next < contentArray.count {
            precacheContentItem(next)
        }
        
        let prev=indexPath.row - 1
        if prev >= 0 {
            precacheContentItem(prev)
        }
    }
    
    func precacheContentItem(index : Int) {
        let item = contentArray[index]
        if item.isVideo {
            // TODO: pre-cache video
        } else {
            guard let url = NSURL(string: item.url) else {
                print("Couldn't convert to NSURL \(item.url)")
                return
            }
            
            let req = ImageCell.URLRequestWithURL(url)
            UIImageView.af_sharedImageDownloader.downloadImage(URLRequest: req) { (response) -> Void in
                print("pre-cache image download completed with \(response)")
            }
        }
    }
}

// Slideshow
extension OneUpController {
    
    func setCurrentContentItemIndex(index : Int, animated : Bool) {
        guard index >= 0 && index < contentArray.count else {
            print("ignoring out of range index \(index). content array size is \(contentArray.count)")
            return
        }
        
        currentContentItemIndex = index
        let indexPath = NSIndexPath(forRow: index, inSection: 0)
        collectionView.selectItemAtIndexPath(indexPath, animated: animated, scrollPosition: .CenteredHorizontally)
    }
    
    func advanceSlideshow() {
        guard currentContentItemIndex + 1 < contentArray.count else {
            // reached the end
            stopSlideshow()
            return
        }
        nextContentItem()
    }
    
    func addPhotoAdvanceTimer() {
        currentSlideshowTimer?.invalidate()
        currentSlideshowTimer = nil
        currentSlideshowTimer = NSTimer.scheduledTimerWithTimeInterval(photoDisplayDuration, target: self, selector: "advancePhotoTimer:", userInfo: nil, repeats: false)
    }
    
    func removeSlideshowTimer() {
        currentSlideshowTimer?.invalidate()
        currentSlideshowTimer = nil
    }
    
    func advancePhotoTimer(timer: NSTimer) {
        currentSlideshowTimer = nil
        advanceSlideshow()
    }
    
    func videoFinishedPlaying(notification: NSNotification) {
        if isRunningSlideshow {
            advanceSlideshow()
        }
    }
    
    func nextContentItem() {
        let next = currentContentItemIndex + 1
        if next < contentArray.count {
            setCurrentContentItemIndex(next, animated: true)
        }
    }
    
    func previousContentItem() {
        let prev = currentContentItemIndex - 1
        if prev >= 0 {
            setCurrentContentItemIndex(prev, animated: true)
        }
    }
    
    func startSlideshow() {
        isRunningSlideshow = true
        
        // if the cell is an image cell, and the image is loaded, then start the timer. Otherwise, wait
        // for the image to be loaded.
        let indexPath = NSIndexPath(forItem: currentContentItemIndex, inSection: 0)
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? ImageCell {
            if cell.imageView.image != nil {
                addPhotoAdvanceTimer()
            }
        }
    }
    
    func stopSlideshow() {
        isRunningSlideshow = false
        removeSlideshowTimer()
    }
}

extension OneUpController : ImageCellDelegate {
    func imageCellImageDidLoad(cell: ImageCell) {
        print("image cell loaded")
        if isRunningSlideshow {
            addPhotoAdvanceTimer()
        }
        cell.delegate = nil
    }
    
    func imageCell(cell: ImageCell, didFailWithError error: NSError) {
        print("image cell failed to load \(error)")
        cell.delegate = nil
    }
}

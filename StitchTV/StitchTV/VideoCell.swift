//
//  VideoCell.swift
//  StitchTV
//
//  Created by Doug Richardson on 11/16/15.
//  Copyright 2015 True Labs, Inc. All rights reserved.
//

import AVFoundation
import UIKit

public class VideoCell: UICollectionViewCell
{
    let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
    let player = AVPlayer()
    let contextPtr = UnsafeMutablePointer<Void>.alloc(1)
    let playerLayer : AVPlayerLayer
    
    
    required public override init(frame: CGRect) {
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.hidden = true
        super.init(frame: frame)
        player.addObserver(self, forKeyPath: "status", options: .New, context: contextPtr)
        contentView.layer.addSublayer(playerLayer)
        contentView.addSubview(loadingIndicator)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        playerLayer = AVPlayerLayer(player: player)
        super.init(coder: aDecoder)
        assert(false)
    }
    
    deinit {
        player.pause()
        player.removeObserver(self, forKeyPath: "status")
        contextPtr.dealloc(1)
    }
    
    public override func prepareForReuse() {
        loadingIndicator.stopAnimating()
        playerLayer.hidden = true
    }
    
    func setVideoURL(videoURL: String)
    {
        guard let url = NSURL(string:videoURL) else {
            print("Invalid video URL \(videoURL)")
            return
        }
        
        let replacementItem = AVPlayerItem(URL: url)
        player.replaceCurrentItemWithPlayerItem(replacementItem)
        
        if player.status == .ReadyToPlay {
            loadingIndicator.stopAnimating()
            playerLayer.hidden = false
        } else {
            loadingIndicator.startAnimating()
            playerLayer.hidden = true
        }
    }
    
    var shouldPlay = false
    
    func play() {
        shouldPlay = true
        if player.status == .ReadyToPlay {
            player.play()
        }
    }
    
    func stop() {
        shouldPlay = false
        player.pause()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        loadingIndicator.frame = contentView.bounds
        playerLayer.frame = contentView.layer.bounds
    }
    
    public override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>)
    {
        if keyPath == "status" {
            if player.status == .ReadyToPlay {
                loadingIndicator.stopAnimating()
                playerLayer.hidden = false
                if shouldPlay {
                    player.play()
                }
            }
        }
    }
}


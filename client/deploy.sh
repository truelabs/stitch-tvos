#!/bin/bash

SITE=$1
shift
BUCKET=$1
shift

usage() {
    cat <<EOF
Usage: deploy.sh <site> <bucket>
Deploy Apple TV client site to a Google Cloud Storage bucket.

Development Deployment:
  ./build.sh site
  ./deploy.sh site gs://tvdev.funsocialapps.com

Production Deployment:
  ./build.sh site
  ./deploy.sh site gs://tv.funsocialapps.com
EOF
}

if [ -z "$SITE" ]; then
    echo "Missing site"
    usage
    exit 1
fi

if [ -z "$BUCKET" ]; then
    echo "Missing bucket"
    usage
    exit 1
fi

set -e

CACHE_ONE_DAY="-h Cache-Control:public,max-age=86400"
CACHE_15_MINUTES="-h Cache-Control:public,max-age=900"
gsutil ls "$BUCKET" > /dev/null || gsutil mb "$BUCKET"
gsutil defacl ch -u AllUsers:R "$BUCKET"

# In order to prevent incomplete apps from being uploaded, first
# upload all the files the web app depends on, and then finally upload
# the main JavaScript file.
gsutil -m $CACHE_ONE_DAY cp -r "$SITE/images" "$BUCKET"
gsutil -m $CACHE_15_MINUTES cp -z js -r "$SITE/js" "$BUCKET"


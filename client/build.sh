#!/bin/bash

OUTDIR=$1
shift

usage() {
    echo "build.sh <outdir>"
    echo "Build Apple TV client site to the given output directory."
}

if [ -z "$OUTDIR" ]; then
    echo "Missing outdir"
    usage
    exit 1
fi

set -e

javascript=(js/es6-promise.js
js/core.js
js/stitchapi.js
js/uuid.js
js/presenter.js
js/templates.js
js/google_analytics.js
js/application.js)

BUILD=$(mktemp -d /tmp/tvos-build-XXXXXX)
mkdir -p "$OUTDIR/js" "$OUTDIR/images"

STEP1="$BUILD/1concatenated.js"
cat ${javascript[@]} > "$STEP1"

cp -r images/*.png "$OUTDIR/images"

FINAL="$OUTDIR/js/application.js"
cp "$BUILD/1concatenated.js" "$FINAL"

echo OK

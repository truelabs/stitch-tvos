//
// AppDelegate.swift
// StitchTV
//
// Copyright 2015 True Labs, Inc. All rights reserved.
//

import AlamofireImage
import TVMLKit
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, TVApplicationControllerDelegate {
    // MARK: Properties
    
    var window: UIWindow?
    
    var appController: TVApplicationController?
    
    #if true
    static let TVBaseURL = "https://storage.googleapis.com/tv.funsocialapps.com/"
    #else
    static let TVBaseURL = "http://localhost:9001/"
    #endif
    
    static let TVBootURL = "\(TVBaseURL)js/application.js"

    // MARK: UIApplication Overrides
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
		
        let window = UIWindow(frame: UIScreen.mainScreen().bounds)
        /*
            Create the TVApplicationControllerContext for this application
            and set the properties that will be passed to the `App.onLaunch` function
            in JavaScript.
        */
        let appControllerContext = TVApplicationControllerContext()
        
        /*
            The JavaScript URL is used to create the JavaScript context for your
            TVMLKit application. Although it is possible to separate your JavaScript
            into separate files, to help reduce the launch time of your application
            we recommend creating minified and compressed version of this resource.
            This will allow for the resource to be retrieved and UI presented to
            the user quickly.
        */
        if let javaScriptURL = NSURL(string: AppDelegate.TVBootURL) {
            appControllerContext.javaScriptApplicationURL = javaScriptURL
        }
        
        appControllerContext.launchOptions["BASEURL"] = AppDelegate.TVBaseURL
        
        if let launchOptions = launchOptions as? [String: AnyObject] {
            for (kind, value) in launchOptions {
                appControllerContext.launchOptions[kind] = value
            }
        }
        
        appController = TVApplicationController(context: appControllerContext, window: window, delegate: self)
		
        return true
    }
    
    func appController(appController: TVApplicationController, evaluateAppJavaScriptInContext jsContext: JSContext) {
        
        //This block  will be called when javascript calls pushMyView()
        let pushMyViewBlock : @convention(block) (NSArray, Bool, Int, NSTimeInterval) -> Void = {
            (jsContentItems : NSArray, slideshow : Bool, index : Int, interval : NSTimeInterval) -> Void in
            dispatch_async(dispatch_get_main_queue()) {
                //pushes a UIKit view controller onto the navigation stack
                let myViewController = OneUpController()
                
                var items : [OneUpController.ContentItem] = []
                for jsItem in jsContentItems {
                    guard let d = jsItem as? NSDictionary else {
                        print("Skipping item that is not a dictionary")
                        continue
                    }
                    
                    guard let url = d.objectForKey("URL") as? String else {
                        print("Didn't get string URL")
                        continue
                    }
                    
                    guard let isVideo = d.objectForKey("isVideo") as? Bool else {
                        print("Didn't get a bool isVideo");
                        continue
                    }
                    
                    items.append(OneUpController.ContentItem(url: url, isVideo: isVideo))
                }
                
                myViewController.contentArray = items
                myViewController.photoDisplayDuration = interval
                myViewController.setCurrentContentItemIndex(index, animated: false)
                if slideshow {
                    myViewController.startSlideshow()
                }
                self.appController?.navigationController.pushViewController(myViewController, animated: true)
            }
        }
        
        //this creates a function in the javascript context called "pushMyView".
        //calling pushMyView() in javascript will call the block we created above.
        jsContext.setObject(unsafeBitCast(pushMyViewBlock, AnyObject.self), forKeyedSubscript: "pushMyView")
    }
    
    // MARK: TVApplicationControllerDelegate
    
    func appController(appController: TVApplicationController, didFinishLaunchingWithOptions options: [String: AnyObject]?) {
        print("\(__FUNCTION__) invoked with options: \(options)")
    }
    
    func appController(appController: TVApplicationController, didFailWithError error: NSError) {
        print("\(__FUNCTION__) invoked with error: \(error)")
    }
    
    func appController(appController: TVApplicationController, didStopWithOptions options: [String: AnyObject]?) {
        print("\(__FUNCTION__) invoked with options: \(options)")
    }
}
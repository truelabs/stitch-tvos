//
//  ImageCell.swift
//  StitchTV
//
//  Created by Brian Ganninger on 11/2/15.
//  Copyright 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import AVFoundation
import AlamofireImage

protocol ImageCellDelegate {
    func imageCellImageDidLoad(cell : ImageCell)
    func imageCell(cell : ImageCell, didFailWithError : NSError)
}

public class ImageCell: UICollectionViewCell {
	var loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
	let imageView = UIImageView()
	let contextPtr = UnsafeMutablePointer<Void>.alloc(1)
    var delegate : ImageCellDelegate?

	
	required public override init(frame: CGRect) {
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
		super.init(frame: frame)
        contentView.addSubview(imageView)
        contentView.addSubview(loadingIndicator)
	}
	
	required public init?(coder aDecoder: NSCoder) {
        assert(false)
		super.init(coder: aDecoder)
	}
    
    deinit {
        contextPtr.dealloc(1)
    }
    
    public override func prepareForReuse() {
        delegate = nil
        imageView.af_cancelImageRequest()
        imageView.image = nil
        loadingIndicator.stopAnimating()
    }
    
    func cancelImageDownload() {
        imageView.af_cancelImageRequest()
        loadingIndicator.stopAnimating()
    }
    
    static func URLRequestWithURL(URL: NSURL) -> NSURLRequest {
        let mutableURLRequest = NSMutableURLRequest(URL: URL)
        mutableURLRequest.addValue("image/*", forHTTPHeaderField: "Accept")
        return mutableURLRequest
    }
	
	func setImageURL(imageURL: String) {
        guard let url = NSURL(string: imageURL) else {
            print("Invalid url \(imageURL)")
            return
        }
     
        loadingIndicator.startAnimating()
        
        let req = ImageCell.URLRequestWithURL(url)
        
        imageView.af_setImageWithURLRequest(req, placeholderImage: nil, filter: nil, imageTransition: .None) {
            (response) -> Void in
            self.loadingIndicator.stopAnimating()
            
            switch response.result {
            case .Success(_):
                self.delegate?.imageCellImageDidLoad(self)
            case .Failure(let error):
                self.delegate?.imageCell(self, didFailWithError: error)
            }
        }
	}
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        loadingIndicator.frame = contentView.bounds
        imageView.frame = contentView.bounds
    }
}
